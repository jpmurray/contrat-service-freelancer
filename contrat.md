# Contrat
Travail effectué par Jean-Philippe Murray pour Pas une compagnie enr.

## 1. Introduction

Bonjour! Je suis _super_ heureux que l’on puisse faire affaire ensemble! Si ce n’était pas du gaspillage, je dirais qu’il faut briser une bouteille de champagne pour commémorer le départ de notre collaboration! J’ose dire que je suis un bon développeur, que mon travail est fait de façon professionnelle, que mes tarifs sont raisonnables et que je suis une personne agréable avec qui travailler. Et je suis certain que vous serez de bons clients aussi!

Je peux lire et écrire quelques langues, mais malheureusement, le « légal » n’est pas une de celle que je maîtrise. C’est probablement le cas pour vous et les frais d’avocats sont très élevés... Il m’importe donc de dire que ce contrat est écrit de façon « humaine » de façon à ce que vous, le client, et moi, le développeur, puissions communiquer nos attentes et nos exigences de manière à ce qu’il n’y ait aucune ambiguïté à savoir qui obtient quoi et quand, et qui fait quoi et quand.

### 1.01 À propos de ce contrat

Ce contrat existe pour protéger tant le développeur que le client. Voici certains points à noter :

1. Les étapes de ce contrat sont à prendre comme des portes; on ne peut en traverser une si l’on n’a pas terminé de passer la première. Par exemple, aucun cadre ni recherche ne seront complété avant la signature du présent contrat; aucun développement ne débutera avant que l’on se soit mis d’accord sur un cadre, etc.
2. Les étapes de ce contrat peuvent sembler très formelles, mais elles sont en place pour de bonnes raisons et sont basées sur mon expérience passée et les pratiques courantes. Mon intention n’est pas d’être inflexible, mais simplement de poser des balises dont nous pourrons tous tirer profit.
3. Je suis plus qu’ouvert à négocier plusieurs aspects du présent contrat. Le contrat est présenté principalement comme une fondation sur laquelle construire.

## 2. Cadre

### 2.01 À propos du cadre

Avant de démarrer le projet, je vais, au fil de nos discussions, développer un cadre pour le projet. Ce cadre prendra la forme d’un document, plus ou moins long selon les cas, qui donnera en détail le travail que je ferai pour vous.

Vous pouvez voir le cadre comme les plans de construction d’une maison : vous ne voulez pas construire un édifice en y allant au hasard sans trop savoir ce que l’on fait, en prenant les décisions qui s’imposent au moment où elles surviennent. Vous vous retrouveriez certainement avec des défauts importants de fabrication qui dévalueront la construction, si ce n’est pas carrément mettre la vie de ses habitants en danger! Évidemment, notre projet ne mettra personne en danger, mais le même genre de logique s’applique : je veux savoir exactement ce que je dois construire afin de bien effectuer le travail.

Le cadre (et le respect de celui-ci) nous protège tous. C’est le document qui pourra nous servir, par exemple, de référence si jamais il y a désaccord sur la présence ou non d’une fonctionnalité au projet.

### 2.02 Ma vision du cadre

Sachez que je ne vois pas le cadre comme entièrement strict. Je m’attends toujours à ce que, dans le cours d’un projet, la vision du client et de son projet évolue, c’est tout à fait normal!

Alors au lieu d’ignorer cette réalité, je me permets de l’affronter directement : vous, en tant que client, avez le droit de vouloir changer des éléments prévu au cadre, mais vous devez garder en tête que tout changement au cadre peut entraîner certains des dépassements de coûts et d’échéanciers.

### 2.03 Apporter des changements au cadre

Facile comme bonjour! Faites-moi signe et l’on en discute, il y a toujours moyen de s’arranger, tant que vous comprenez que des dépassements seront probablement à prévoir.

## 3. Développement

### 3.01 Modalités

Je ne travaille jamais sans système de contrôle de version. Si vous vous attendez à ce que je travaille avec une base de code existante dont vous êtes le propriétaire, et que vous n’avez aucune stratégie de contrôle de version d’implantée, c’est avec plaisir que je peux le faire pour vous, moyennant certains frais.

Je ne travaillerai pas sur vos serveurs et cela devrait aller de soi. Si un client me faisait des misères sur les paiements et que j’avais fait tout le travail sur ses serveurs, il se retrouve avec une copie de tout mon travail et moi je n’ai rien pour payer mon loyer, et ce n’est vraiment pas souhaitable! Je travaillerai donc localement et utiliserai mes propres serveurs afin de vous soumettre mon travail pour approbation. Je ne livrerai le code sur vos serveurs qu’au moment où le paiement est réglé.
Je vous ferai parvenir les liens pour les approbations aux moments convenus en début de projet. Typiquement, les étapes d’approbation sont toutes les deux semaines, mais la longueur du projet peut influencer ce calendrier.

Tout le travail sera effectué et approuvé selon le cadre que nous nous serons fixé.

## 4. Entretient

### 4.01 Chasse aux bogues et autres problèmes

Malheureusement, des bogues surviennent lors du développement d’application et autant que je voudrais promettre un produit à 100 % libre de tout bogue, c’est impossible de le faire et quelqu’un qui le ferait serait profondément malhonnête.

Cela ne veut cependant pas dire que je ne livrerai pas un code qui est fonctionnel et de bonne qualité. Je ferai le mieux que je peux pour vous offrir un produit sans bogue et travaillerai fort pour régler ceux qui pourraient survenir.

À ce sujet, notez que :

1. Je corrigerai tout bogue qui est le résultat d’une erreur de ma part, et ce gratuitement.
2. Je n’assume aucune responsabilité pour des bogues qui sont dus à du code que je n’aurais pas écrit (ex. : provenant de WordPress ou Drupal, en tant que gestionnaire de contenu dans le projet) et je ne serai pas tenu, le cas échéant, de résoudre ces bogues. Si vous ne voulez pas attendre que les responsables de ces codes règlent le problème par eux-mêmes, je serai bien heureux de m’y attarder au tarif fixé par notre entente.
3. Les bogues ne sont pas la même chose qu’un changement au cadre.

### 4.02 Questions juridiques

Je ne pourrai en aucun cas être tenu responsable de toutes pertes (profits, ventes, etc.) qui seraient dues à un bogue dans le code que j’écrirai. Il n’est malheureusement pas possible pour moi d’offrir ce genre de garantie, mais je ferai tout en mon possible pour régler les bogues de la manière la plus rapide dont il m’est possible d’agir pour prévenir d’autres pertes.

## 5. Paiements

### 5.01 Fonctionnement

Une fois que nous nous sommes entendus sur un cadre et un prix, le prochain point à travailler est celui du paiement. Le tout est en fait très simple et le raisonnement derrière cette façon de faire est de minimiser les risques pour moi, en tant que développeur, et vous, le client, et nous offrir une tranquillité d’esprit tout au long de notre collaboration.

Typiquement, vous, le client, me verserez un paiement à trois (3) moments différents. Premièrement, j’exigerai le tiers du prix total à la signature du contrat. Je demanderai le second tiers au moment où je vous montre le travail final pour approbation. Finalement, le troisième tiers devra m’être versé au moment où je vous livre le travail (ex. : je passe le code « en production », vous le livre par courriel, etc.). Ces termes pourraient cependant être revus dans le cas où la longueur estimée du projet ne se prêterait pas à ce genre de calendrier.

Je ferai de mon mieux pour être accommodant, mais comprenez que je suis très stricte quand il s’agit des questions de paiements, tout comme vous l’êtes probablement avec vos propres clients.

Sauf arrangements différents, je préfère être payé par chèque ou argent comptant.

### 5.02 Cas extrêmes et remboursements

Les deux partis (moi, le développeur, et vous, le client) pourront, à tout moment, se retirer du projet. Cela pourrait arriver, par exemple, si le cadre du projet change à un point que je ne suis plus confiant de mes habiletés à effectuer le travail de manière qui serait responsable financièrement de ma part. Ce genre de moments, extrêmement rare, n’arrivent qu’après plusieurs échanges entre nous et face à une intransigeance de la part du client à accommoder financièrement les conséquences du changement au cadre. En bref, soyez gentil avec moi et je ferai avec plaisir le travail pour lequel vous payez.

Si jamais nous devons arrêter notre collaboration en plein projet, je suis sûr que nous pourrons trouver un terrain d’entente dans lequel je pourrais être payé pour la portion du travail qui a été effectué et que vous recevrez la portion du travail pour laquelle vous avez payé... Ou toute autre entente que nous trouverons raisonnable.

Je ne livrerai, sous aucun prétexte, le code de mon travail au client sans avoir reçu une compensation quelconque. À mon sens, il en va de soi : vous ne recevrez pas de marchandise sans l’avoir payé. Je ne crois pas que ni l’un ni l’autre d’entre nous n’est déraisonnable, alors encore une fois, tout cela ne se résume qu’à l’idée que si vous êtes gentil avec moi, je ferai tout en mon possible pour faire de vous un client heureux.

Il n’y a qu’une seule exception à la règle : tout temps qui est facturé sous la catégorie « gestion de projet » (ex. : rencontres et communications avec le client, préparation de documents, etc.) est non remboursable. Si pour peu importe la raison, nous décidons de rompre notre collaboration, vous devrez toujours vous acquitter des frais de gestion de projet. Je suis désolé, mais j’ai eu affaire à trop de personnes qui passent un temps déraisonnable sur la planification pour se désister à la toute fin.

### 5.03 Questions juridiques

Comme mentionné auparavant, je suis très strict au sujet du paiement. Recevoir les paiements pour le travail que je fais de la façon convenue est le seul moyen de continuer à faire ce que j’aime dans la vie. En espérant ne pas avoir l’air trop méchant, je me dois quand même de le dire : si pour une raison ou une autre, vous, le client, refusez de me payer et que j’ai complété ma part du contrat tel que défini dans le cadre, je devrai prendre des actions légales afin de m’assurer d’être payé en bonne et due forme. Dans un pareil cas, si le règlement est en ma faveur, vous consentez à me rembourser les frais légaux encourus dans le processus.

## 6. Propriété intellectuelle

### 6.01 Qui est propriétaire du code après le projet?

En ce qui a trait aux éléments statiques (images, textes, achats de thèmes, etc.), vous l’êtes. En ce qui a trait au code et aux fonctionnalités, je le suis.

Votre paiement pour le travail que j’effectue est normalement un paiement pour une licence d’utilisation du code que j’écris pour utilisation sur un site spécifié au moment de notre entente (sauf si je vous donne expressément la permission d’utiliser le code sur d’autres sites ou selon la licence qui est associée au code que je vous donne le permet).

Si vous voulez avoir un arrangement différent, je suis prêt à négocier.

### 6.02 Implications de la communauté _open source_

Je suis très engagé aux valeurs du mouvement _open source_ (logiciels libres). En travaillant avec moi, vous profitez déjà, sans coûts additionnels, du bénéfice du travail effectué par d’autres développeurs de la communauté _open source_ : des logiciels comme, par exemple, WordPress ou Drupal et tous les modules qui y sont associés proviennent tous d’effort _open source_.

Je crois qu’il est un devoir de redonner à cette communauté qui nous permet d’accomplir autant et la façon qu’ont les développeurs de le faire est ordinairement d’extraire en partie le code de certaines fonctionnalités que des clients ont commandées et de le partager à la communauté dans l’espoir que ce code puisse leur être utile à d’autres.

Comme je suis propriétaire du code source du travail que j’effectue pour vous, je me réserve le droit d’extraire du code du projet et de le transmettre au domaine public et à la communauté _open source_. Ne vous inquiétez pas, je ne donnerai en aucun cas des données qui seraient sensibles pour votre entreprise (mot de passe, information de paiement, imagerie spécifique à votre entreprise, secrets industriels et commerciaux, etc.). Dans un pareil cas, je prendrai la fonctionnalité que j’ai bâtie pour vous que je considère d’intérêt pour la communauté et, si possible, l’élargirai afin de la rendre attrayante pour le plus de gens possibles et que la fonctionnalité ne soit pas spécifiquement rattachée à votre champ de compétence ou industrie.

Si je suis en mesure de le faire, et choisis de le faire, vous pourrez compter sur trois choses. Premièrement, je vous informerai que je choisis de le faire; deuxièmement, je vous donnerai crédit d’une façon ou d’une autre pour avoir contribué financièrement et; troisièmement, je ferai tout travail afin de rendre neutre le code avant le déploiement dans la communauté sur mon propre temps. Les valeurs du logiciel libre parmi celles auxquels je crois, et je ne m’attends pas à ce que vous financiez le travail nécessaire afin de rendre le code acceptable à la communauté.

Il y a beaucoup plus de chances que cela ne vous affecte pas outre mesure. Mais si vous avez des questions à propos de la communauté _open source_, votre projet, ou mes intentions, soyez absolument libre de m’en parler.

Ceci est un exemple de contrat standard que j'utilise avec mes clients comme travailleur
autonome en développement web. Il peut être amandé et/ou complémenté au gré des clients et des projets.
Ce n'est qu'un cavenas sur lequel je me base.

## Mention légale

Je ne suis ni un avocat, ni un juriste. Ce contrat est disponible comme tel,
sans aucune restriction. Cependant, je n'assume aucune responsabilité quelle
qu'elle soit sur peu importe le sujet.

## Contributions

Je suis plus qu'ouvert aux contributions si vous en avez!